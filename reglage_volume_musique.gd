extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var m_volume= float()
var a_volume= float()
var b_volume= float()
	
const max_volume=float(10)
const min_volume=float(-10)

func _ready():
	
	m_volume=AudioServer.get_bus_volume_db(1)  # musique
	b_volume=AudioServer.get_bus_volume_db(2)  # bruitage
	a_volume=AudioServer.get_bus_volume_db(4)  # ambiance
	
	# Called when the node is added to the scene for the first time.
	# Initialization here

#		AudioServer.set_bus_volume_db(1,0)  pour rinitialiser le volume (mais on veut le conserver entre les scenes;..
		
		
func _process(delta):
	
	
	if Input.is_action_pressed("ui_page_down") :
		if m_volume>=min_volume  :
			m_volume -= 3*delta
			b_volume += 3*delta
			a_volume += 3*delta
			
			AudioServer.set_bus_volume_db(1,m_volume)  # musique
			AudioServer.set_bus_volume_db(2,b_volume)  # bruitage
			AudioServer.set_bus_volume_db(4,a_volume)  # ambiance
		
			print(m_volume)
		else :
			$limite_volume.play()
		
	if Input.is_action_pressed("ui_page_up"):
		if m_volume<=max_volume  :
			m_volume += 3*delta
			b_volume -= 3*delta
			a_volume -= 3*delta
			
			AudioServer.set_bus_volume_db(1,m_volume)  # musique
			AudioServer.set_bus_volume_db(2,b_volume)  # bruitage
			AudioServer.set_bus_volume_db(4,a_volume)  # ambiance
		
			print(m_volume)
		
		else :
			$limite_volume.play()
	
	
	
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
