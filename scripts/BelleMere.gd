extends RigidBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	var angle = rand_range(1, 20)
	var velo = rand_range(30, 100)
	print(angle)
	angular_velocity = 5
	set_linear_velocity(Vector2(get_linear_velocity().x, -velo))
	$AudioStreamPlayer.play()
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
