extends Node2D

var canDead = true
var canWin = true
var score = 0
var perdu = int(0)

var timer_tuto = int(0)
var timer_mort = float(2)  # durée d'attente en secondes
var best_score = 0
var scoreActuel
const score_filepath = "user://bestscore.data"

func _ready():
	get_tree().paused = false
	# Called when the node is added to the scene for the first time.
	$sonLevel.play()
	$Pause.hide()
	$GameOver.hide()
	$Victoire.hide()
	
	AudioServer.set_bus_mute(2,false)     # bruitage activés
		
	randomize()
	$eclair/eclair1.hide()

	# Gestion score
	var score_file = File.new()
	if not score_file.file_exists(score_filepath): return
	# on charge le meilleur score
	score_file.open(score_filepath, File.READ)
	best_score = score_file.get_var()
	score_file.close()

	pass

func _process(delta):
	
	$HUD/Sprite/Gaz.value = int($boule_RigidBody2D.reserve_gaz)
	timer_tuto += 1*delta
	if timer_tuto >= 5 :
		$tutoriel_Sprite.hide()
	
	
	if(canDead):
		score = abs(round($Parallax_bg.offhuit))

	$HUD/Sprite/Score.text = str(score)
	if($boule_RigidBody2D.position.x < -20):
		$boule_RigidBody2D.hide()
		gameover()
#		perdu += 1
		
#      INFO :
	# si perdu = 0  :  on joue
	# si perdu = 1  : on vient juste de perdre
	# si perdu >1   : on reste dans l'état perdu....
#	if perdu == 1 :
#		$sonGameOver.play()

	if Input.is_action_just_pressed("ui_quit"):
		get_tree().change_scene("res://scenes/Menu.tscn")
	
	if Input.is_action_just_pressed("ui_pause"):
		get_tree().paused = true
		$pauseOn.play()
		$Pause.show()
		
		
	#  eclair et modif pluie ==========================
		
#		on pourrait modifier aléatoirement le lieu des eclairs, et le son de l'éclair lancé 
#	 et le volume du  bus eclair .....	(en fonction de la densité eclair par exemple)  
		
	var densite_eclair = float(0.07)  # 0.05 ok et si proche tres  0 alors très peu d'eclairs, et proche 0.1 alors enéormément d'éclairs
	var duree_proba_eclair =float(0.9) # si très proche de 9.99 alors très long... 0.9 ok
	var proba_eclair = randf()
#	print("proba eclair = " + str(proba_eclair))
		
	if proba_eclair < densite_eclair*delta:     # correction des proba en fonction du FPS 
		$bruit_eclair.play()
		$eclair/eclair1.show()
#		print ("ECLAIR")
	elif proba_eclair > duree_proba_eclair :
		$eclair/eclair1.hide()
#		print ("FIN  ECLAIR")
#	else :
#		print ("RIEN pour les eclairs")
	
		
	if perdu >= 1 :			#             0 = en vie             1 et plus  = mort
		timer_mort -= delta                # donne un delai de x secondes
		if timer_mort <=0 :
			$boule_RigidBody2D.hide()
			$bruit_effleure_eau.stop()
			gameover()
	pass
	
	#   _________________________________________________________________
	
func gameover():
	if(canDead):
		$GameOver.show()
		$timerDecalageMusique.start()
		$sonGameOver.play()
		AudioServer.set_bus_mute(2,true) # bruitage coupés
	canDead = false

func _on_Vagues_body_entered(body):
	if(body.is_in_group('mongolfiere')):
		# Nacelle coule

		$bruit_Mort_eau.play()
		$bruit_effleure_eau.play()
		perdu +=1               # et on lance gameover  dans script main
	
	if(body.is_in_group('larguer')):
		# Navelle coule
		print("body queue free")
		body.queue_free()
	pass # replace with function body


func _on_timerDecalageMusique_timeout():
	#  on met un fade pour la musique level ????
	$sonLevel.stop()
	pass # replace with function body


func _on_vaguesArea2D_area_entered(area):     # inutile : ne fonctionne pas
#	$bruit_Mort_eau2.play()
	print('entree dans vaguesArea2d')
	pass # replace with function body


func _on_pause_pressed():
	get_tree().paused = true
	$pause_popup.show()
	pass # replace with function body


func _on_Button_pressed():
	$pause_popup.hide()
	get_tree().paused = false
	pass # replace with function body

func atterissageReussi():
	if(canWin):
		get_tree().paused = true
		$sonVictoire.play()
		$Victoire.show()
	
		# lecture du score actuel
		var score_file = File.new()
		if not score_file.file_exists(score_filepath): return
		score_file.open(score_filepath, File.READ)
		scoreActuel = score_file.get_var()
	
		best_score = score
		if( scoreActuel < best_score):
			scoreActuel = best_score
			var file = File.new()
			file.open(score_filepath, File.WRITE)
			file.store_var(best_score)
			file.close()
		$Victoire/bestScore/Label.text = str(scoreActuel)
		canWin = false
	pass