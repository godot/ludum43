extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_jouer_pressed():
	$bruit_clic_AudioStreamPlayer.play()
	get_tree().change_scene("res://MainPhysique.tscn")
	pass # replace with function body


func _on_credit_pressed():
	$bruit_credit_AudioStreamPlayer.play()
	get_tree().change_scene("res://scenes/Credits.tscn")
	pass # replace with function body


func _on_quit_pressed():
	get_tree().quit()
	pass # replace with function body
