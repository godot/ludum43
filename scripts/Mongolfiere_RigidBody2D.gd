extends RigidBody2D

const gravity = Vector2(0, 98) # The default gravity, you can use your own.
const SPEED = 500
var turbo = 2

func _integrate_forces(state):
	var dt = state.get_step()
	var velocity = state.get_linear_velocity()
	var some_normal_vector = Vector2(0, 1) 

	var direction = some_normal_vector * gravity.length()

	state.set_linear_velocity(velocity + direction * dt)

func _ready():
#	set_use_custom_integrator(true)
	set_process_input(true)
	set_physics_process(true)
	
func _physics_process(delta):
	if(Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down") ):
		if Input.is_action_pressed("ui_left"):
			print('left')
#			set_linear_velocity(Vector2(get_linear_velocity().y, -150))
		elif Input.is_action_pressed("ui_right"):
			$"../Parallax_bg"/sky.motion_scale = Vector2(turbo, "1")
			$"../Parallax_bg"/clouds.motion_scale = Vector2(turbo, "1")
			for i in range(10, 0, +1):
				print(i)


#			set_linear_velocity(Vector2(get_linear_velocity().y, 150))
#			set_applied_force(Vector2(200,0))
			#set_linear_velocity(Vector2(+100,0))
		elif Input.is_action_pressed("ui_up"):
#			set_applied_force(Vector2(0,-200))
			set_linear_velocity(Vector2(get_linear_velocity().x, -150))
		elif Input.is_action_pressed("ui_down"):
			set_linear_velocity(Vector2(get_linear_velocity().x, 150))
	else:
		set_applied_force(Vector2(0,0))
#		$"../Parallax_bg"/sky.motion_scale = Vector2("0.2", "1")
#		$"../Parallax_bg"/clouds.motion_scale = Vector2("0.2", "1")
	pass

