extends StaticBody2D

export (int) var Speed = 100 

func _ready():
	set_process(true)
	pass

func _process(delta):
	position = Vector2(position.x - Speed * delta, position.y)
	pass


func _on_Area2D_body_entered(body):		# A CORRIGER : seulement si c'est la mongolfiere, et pas les sacs par exemple
	print("la nacelle arrive")
	$choc_montagne_.play()

	pass # replace with function body
