extends Node2D

var ObstacleInstance = load("res://scenes/parties_de_decors/plateformeDETAIL_StaticBody2D.tscn")
var plateforme_longue = load("res://scenes/parties_de_decors/plateforme_longue_DETAIL_StaticBody2D.tscn")
var Mountain1Instance = load("res://scenes/parties_de_decors/montagne1_StaticBody2D.tscn")
var Mountain2Instance = load("res://scenes/parties_de_decors/montagne2_StaticBody2D.tscn")
var Pic1Instance = load("res://scenes/parties_de_decors/pic1_StaticBody2D.tscn")
var Pic2Instance = load("res://scenes/parties_de_decors/pic2_StaticBody2D.tscn")
var Tornade = load("res://scenes/parties_de_decors/tornade.tscn")
var Nuage_obstacle = load("res://scenes/parties_de_decors/nuage_obstacle.tscn")

var nextSpawnTime = 5;
var spawnTime = 2 #5
var aide_spawn = float(0)

func _ready():
	set_process(true)
	
	randomize()
	nextSpawnTime = spawnTime

func _process(delta):
	if nextSpawnTime <= 0:
		var proba = randf()
		print("probability spawn = " + str(proba))
		
		if proba < (0.33 +aide_spawn)*delta*10:
			aide_spawn=0
			var probaChoice = randf()
#			probaChoice = 0.8
			print("probability asset = " + str(proba))
			

			if probaChoice < 0.05 :
				print("Spawn mountain 1")
				var obstacleItem = Mountain1Instance.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
			elif  probaChoice < 0.20 :
				print("Spawn NUAGE obstacle")
				var obstacleItem = Nuage_obstacle.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
				obstacleItem.connect("nuage_noir", $"../boule_RigidBody2D", "courantnuagenoir") 
				print("Spawn mountain 2")
			elif  probaChoice < 0.35 :
				var obstacleItem = Mountain2Instance.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
			elif  probaChoice < 0.45 :
				print("Spawn pic 1")
				var obstacleItem = Pic1Instance.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
			elif  probaChoice < 0.6 :
				print("Spawn pic 2")
				var obstacleItem = Pic2Instance.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
			elif probaChoice < 0.75 :
				print("Spawn island")
				var obstacleItem = ObstacleInstance.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
			elif probaChoice < 0.9 :
				print("Spawn LONG_island")
				var obstacleItem = plateforme_longue.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
				obstacleItem.connect("poser", $"../../Main", "atterissageReussi") 
			elif probaChoice < 0.95 :
				print("Spawn tornade")
				var obstacleItem = Tornade.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
				obstacleItem.connect("tornader", $"../boule_RigidBody2D", "courantTornade") 
			else :
				print("Spawn NUAGE obstacle")
				var obstacleItem = Nuage_obstacle.instance()
				obstacleItem.position = position
				add_child(obstacleItem)
				obstacleItem.connect("nuage_noir", $"../boule_RigidBody2D", "courantnuagenoir") 
		else :
			aide_spawn += 0.05
			print(aide_spawn)
		
				
		nextSpawnTime = spawnTime
	else:
		nextSpawnTime -= delta