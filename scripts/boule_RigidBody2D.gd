extends RigidBody2D

var BelleMere = load("res://scenes/BelleMere.tscn")
var sac_largue = load("res://scenes/sac_largue.tscn")
var Chien = load("res://scenes/Chien.tscn")
var Mamie = load("res://scenes/Mamie.tscn")
var perso_fille = load("res://scenes/perso_fille.tscn")
var perso_Detente = load("res://scenes/perso_Detente.tscn")
var perso_plongeon = load("res://scenes/perso_plongeon.tscn")

# portance représente l'effet de l'air chaud  : doit dépendre de la temperature !!
# et la temperature depend de l'effet de la torche, et refroidit lentement..
var portance = float()
# portance_100deg   =  11 000  sinon   monte ou descend trop !!
var portance_100deg = float(-11000)
var coef_portance = float(0.01)
# var coef_portance = float(0.01)               valeur qui fonctionne

var temperature_ext= float (20)
var temperature_ballon= float (100)
# regler l'isolation_ballon pour ajuster la jouabilité (perte de température)  : 0.2   fonctionne bien
var isolation_ballon = float(0.2)

# reglage des gaz du bruleur entre 5 et 9  (consomme réserve gaz)
# et en dessous (1 à 4) on laisse s'echapepr l'air chaud (ne consomme pas)
var chauffe = int(5)
var chauffe_min = int(1)
var chauffe_max = int(9)

# regler le coef_chauffe pour ajuster la jouabilité     =3  fonctionne bien
var coef_chauffe= float(3)

var reserve_gaz = float(50)

var altitude = float()
# au dessus de limite altiutde max  :  problème de givre et equivaut à ajout_poids =   poids_givre
var altitude_max = float(0)

var ajout_poids = float(0)
var poids_givre= float(9000)
var ajout_givre = float()

var nb_largage = int(0)
var nb_sac = int(4)
var nb_personnes = int(1)
var nb_animaux = int(2)

var duree_pressed_up=int(1)
var duree_pressed_down=int(1)
var duree_up=int(0)
var duree_down=int(0)

var tornade_impulse_Vector2=Vector2(-100,-5000)   # valeurs correctes : (-100,-10000)
var tornade_offset_impulse=Vector2(0,0)

var nuage_noir_impulse_Vector2=Vector2(0,-1000)   # valeurs correctes : (0,-5000)
var nuage_noir_offset_Vector2=Vector2(0,0) 


var larguer_impuse_Vector2=Vector2(0,-3000)
var larguer_impuse_offset=Vector2(0,0)

#var vector_scale=vector2(1,1)

func _process(delta):
	#print(chauffe)

#	if(reserve_gaz < 0):				pas besoin ici !!!
#		chauffe = 1                  non : corrigé plus bas...
# ajustement de la chauffe
#	if(Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_down")  ):
	if Input.is_action_just_pressed("ui_up") or (Input.is_action_pressed("ui_up")and duree_up>=duree_pressed_up):
			chauffe += 1
			duree_up=0
			if reserve_gaz >= 0 and chauffe >= 6 :
				$bruit_continu_flamme1.stop()
				$bruit_continu_flamme1.play()
			if reserve_gaz >= 0  and chauffe >= 6 and chauffe <= 9 :
				$bruit_court_allume_flamme.stop()
				$bruit_court_allume_flamme.play()
			if chauffe <=5 :
				$bruit_court_eteint_flamme2.play()
				
			if chauffe >9 :									# dernier if !!!
				chauffe = 9
				
	elif Input.is_action_pressed("ui_up"): # pour activer la touche au bout d'un certain temps
		duree_up+=1*delta

	if Input.is_action_just_pressed("ui_down") or (Input.is_action_pressed("ui_down")and duree_down>=duree_pressed_down) :
			chauffe -= 1
			duree_down = 0
			if chauffe == 4 :
				$bruit_continu_evacuation2.stop()
				$bruit_continu_evacuation2.play()
			if  chauffe >= 1 and chauffe <= 4 :
				$bruit_court_debut_evacuation.play()
			if reserve_gaz >= 0  and chauffe >= 5 :
				$bruit_court_allume_flamme2.play()
				
			if chauffe <1 :									# dernier if !!!
				chauffe = 1
	elif Input.is_action_pressed("ui_down"):		# pour activer la touche au bout d'un certain temps
		duree_down+=1*delta
		
		
	if reserve_gaz <= 0 :
			chauffe = min(chauffe, 5)
		
	# limitation chauffe entre min et max
	if chauffe > chauffe_max :
		chauffe = chauffe_max
		# faire un bruit ??
	elif chauffe < chauffe_min :
		chauffe = chauffe_min
		# faire un bruit ??
		
#	print(chauffe)
#	print(temperature_ballon)

	$flamme_sprite1.hide()
	$Sprite_evacuation1.hide()
	$flamme_sprite2.hide()
	$Sprite_evacuation2.hide()
	$flamme_sprite3.hide()
	$Sprite_evacuation3.hide()
	$flamme_sprite4.hide()
	$Sprite_evacuation4.hide()


	if chauffe == 6 :
		$flamme_sprite1.show()
		$bruit_continu_evacuation2.stop()
	elif chauffe == 7 :
		$flamme_sprite2.show()
		$bruit_continu_evacuation2.stop()
	elif chauffe == 8 :
		$flamme_sprite3.show()
		$bruit_continu_evacuation2.stop()
	elif chauffe == 9 :
		$flamme_sprite4.show()
		$bruit_continu_evacuation2.stop()

	elif chauffe == 4 :
		$Sprite_evacuation1.show()
		$bruit_continu_flamme1.stop()
	elif chauffe == 3 :
		$Sprite_evacuation2.show()
		$bruit_continu_flamme1.stop()
	elif chauffe == 2 :
		$Sprite_evacuation3.show()
		$bruit_continu_flamme1.stop()
	elif chauffe == 1 :
		$Sprite_evacuation4.show()
		$bruit_continu_flamme1.stop()
		
	else :
		$bruit_continu_flamme1.stop()
		$bruit_continu_evacuation2.stop()
		
		
	temperature_ballon = temperature_ballon + delta*  ( chauffe*coef_chauffe - (temperature_ballon - temperature_ext) * isolation_ballon )
	
	if temperature_ballon > 104 :
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/CHAUD_spr_mongolfiere3.show()
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/FROID_spr_mongolfiere1.hide()
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/spr_mongolfiere2.hide()
#		print("temp haute")

	elif temperature_ballon < 97 :
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/FROID_spr_mongolfiere1.show()
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/spr_mongolfiere2.hide()
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/CHAUD_spr_mongolfiere3.hide()
	else :
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/spr_mongolfiere2.show()
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/FROID_spr_mongolfiere1.hide()
		$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/CHAUD_spr_mongolfiere3.hide()

		
	
	if chauffe >5 :
		reserve_gaz -= (chauffe-5) * delta 
	
	portance = portance_100deg * (1+(temperature_ballon-100) * coef_portance )
	
	
	#   LARGAGE-----------------------------------------------------
	
	if Input.is_action_just_pressed("ui_larguer"):
		var stage_node = get_parent()
		
		if nb_largage < nb_sac :
			nb_largage += 1
			ajout_poids = ajout_poids -20		# -20 poids ideal
			apply_impulse(larguer_impuse_offset,larguer_impuse_Vector2)		# impulsion vers le haut lors du larguage
			if nb_largage==1:
				$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/sac1.hide()
			if nb_largage==2:
				$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/sac2.hide()
			if nb_largage==3:
				$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/sac3.hide()
			if nb_largage==4:
				$PinJoint2D/RigidBody2D/PinJoint2D/nacelle_RigidBody2D/sac4.hide()
			var s = sac_largue.instance()
			s.add_to_group("larguer")
			s.position = position  + Vector2(-20, 110)
			stage_node.add_child(s)
		elif nb_largage < nb_sac + nb_animaux :
			nb_largage += 1
			ajout_poids = ajout_poids -30		# -30 poids ideal
			apply_impulse(larguer_impuse_offset,larguer_impuse_Vector2)		# impulsion vers le haut lors du larguage
			var b = Chien.instance()
			b.add_to_group("larguer")
			b.position = position  + Vector2(-20, 110)
			stage_node.add_child(b)
		elif nb_largage < nb_sac + nb_animaux + nb_personnes :
			nb_largage += 1
			ajout_poids = ajout_poids -40		# -40 poids ideal
			apply_impulse(larguer_impuse_offset,larguer_impuse_Vector2)		# impulsion vers le haut lors du larguage
			var b = BelleMere.instance()
			b.add_to_group("larguer")
			b.position = position  + Vector2(-20, 110)
			stage_node.add_child(b)
		elif nb_largage < nb_sac + nb_animaux + nb_personnes *2 :
			nb_largage += 1
			ajout_poids = ajout_poids -40		# -40 poids ideal
			apply_impulse(larguer_impuse_offset,larguer_impuse_Vector2)		# impulsion vers le haut lors du larguage
			var b = Mamie.instance()
			b.add_to_group("larguer")
			b.position = position  + Vector2(-40, 110)
			stage_node.add_child(b)
		elif nb_largage < nb_sac + nb_personnes + nb_animaux + nb_personnes*3 :
			nb_largage += 1
			ajout_poids = ajout_poids -40		# -40 poids ideal
			apply_impulse(larguer_impuse_offset,larguer_impuse_Vector2)		# impulsion vers le haut lors du larguage
			var b = perso_plongeon.instance()     #  probleme a resoudre (lien avec la scene ??)
			b.add_to_group("larguer")
			b.position = position  + Vector2(-40, 110)
			stage_node.add_child(b)
		elif nb_largage < nb_sac + nb_personnes + nb_animaux + nb_personnes*4 :
			nb_largage += 1
			ajout_poids = ajout_poids -40		# -40 poids ideal
			apply_impulse(larguer_impuse_offset,larguer_impuse_Vector2)		# impulsion vers le haut lors du larguage
			var b = perso_Detente.instance()      #  probleme a resoudre (lien avec la scene ??)
			b.add_to_group("larguer")
			b.position = position  + Vector2(-40, 110)
			stage_node.add_child(b)
		elif nb_largage < nb_sac + nb_personnes + nb_animaux + nb_personnes*5 :
			nb_largage += 1
			ajout_poids = ajout_poids -40		# -40 poids ideal
			apply_impulse(larguer_impuse_offset,larguer_impuse_Vector2)		# impulsion vers le haut lors du larguage
			var b = perso_fille.instance()        #  probleme a resoudre (lien avec la scene ??)
			b.add_to_group("larguer")
			b.position = position  + Vector2(-40, 110)
			stage_node.add_child(b)
		else :
			$bruit_court_je_suis_seul.play()
			print(" tout seul a rester ")
	
	
	
#	print(reserve_gaz)
	
# calcul altitude :
	altitude = get_global_position().y
#	print(altitude)
	
	
	if altitude < altitude_max :
		ajout_givre = poids_givre
	else :
		 ajout_givre = 0
	
	
#	print(ajout_poids)
	
	
func _physics_process(delta):
	
	set_applied_force(Vector2(0,portance + ajout_poids + ajout_givre))
#	add_force(Vector2(0,-5000), Vector2(0,-10000))

	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func courantTornade():
	print("fct tornade on monte")
	apply_impulse(tornade_offset_impulse,tornade_impulse_Vector2)
	pass
	
func courantnuagenoir():
	print("fct NUAGE NOIR on monte")
	apply_impulse(nuage_noir_offset_Vector2 ,nuage_noir_impulse_Vector2)
	pass

func _on_nacelle_RigidBody2D_body_entered(body):
	print("collission de la nacelle")
	$bruit_court_choc_montagne.play()
	pass # replace with function body
