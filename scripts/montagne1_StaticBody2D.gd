extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export (int) var Speed = 100 

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	set_process(true)
	pass

func _process(delta):
	position = Vector2(position.x - Speed * delta, position.y)
	pass
