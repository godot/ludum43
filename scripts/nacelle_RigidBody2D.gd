extends RigidBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"


var thrust = Vector2()
var thrust_position = Vector2()
var poussee = 40


func _ready():
	
	thrust_position = Vector2(0,100)
	
	
func _physics_process(delta):
	
	
	if (Input.is_action_just_pressed("ui_left") or Input.is_action_just_pressed("ui_right")  ):
		$bruit_continu_helice.play()
	# or Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down")
	
	
	if Input.is_action_pressed("ui_left"):
		thrust = Vector2(-poussee,0)			
		$avance_sprite.hide()
		$recule_sprite.show()
	elif Input.is_action_pressed("ui_right"):
		thrust = Vector2(poussee,0)		
		$avance_sprite.show()
		$recule_sprite.hide()
	else :
		thrust = Vector2(0,0)
		$avance_sprite.hide()
		$recule_sprite.hide()
		$bruit_continu_helice.stop()


	pass

	# add_force(thrust_position, thrust)
	apply_impulse(thrust_position, thrust)
			
	
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_nacelle_RigidBody2D_body_entered(body): # Ne fonctionne PAS
	print("collisiiionnnnnnnn")
	if body.is_in_group("pour_bruit_choc"):
		print("choc avec bruit")
		$bruit_court_choc_montagne.play()
		
	pass # replace with function body


func _on_nacelle_bruit_zone_body_entered(body):
	print("collisiiionnnnnnnn AREA")
	$bruit_court_choc_montagne.play()
	if body.is_in_group("pour_bruit_choc"):
		print("choc avec bruit")
		$bruit_court_choc_montagne.play()
	pass # replace with function body
