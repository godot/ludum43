extends ParallaxBackground

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var offhuit = 0

func _ready():
	set_process(true)
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	offhuit -= 100 * delta
	set_scroll_offset(Vector2(offhuit, 0))
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass
