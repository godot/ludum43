extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export (int) var Speed = 100 

signal poser

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	set_process(true)
	pass

func _process(delta):
	position = Vector2(position.x - Speed * delta, position.y)
	pass
	


func _on_pisteAterrissage_body_entered(body):
	print("atterrisage")
	if(body.is_in_group("mongolfiere")):
		emit_signal("poser")
	pass # replace with function body
